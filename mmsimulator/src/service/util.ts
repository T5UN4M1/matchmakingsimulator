export const clamp = (val: number, min: number, max: number): number => Math.max(Math.min(val, max), min);

export const getRng = (min: number, max:number):number => Math.floor(Math.random() * (max - min + 1) ) + min;

export const shuffleArray = (array:any[]): any[] => {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        const temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}