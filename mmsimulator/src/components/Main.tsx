import {MatchMakerSystem, MMParams} from "../model/MatchMakerSystem";
import {useEffect} from "react";


export const Main = () => {
    useEffect(() => {
        const mm = new MatchMakerSystem(new MMParams(6,2000));
        for(let i=0;i<100;++i) {
            mm.playIterationWowsS72022();
        }
        console.log(mm);
    })

    return (
        <div>
            TEST MAIN
        </div>
    )
}