import React from "react";

const prStyleTreshold = [ // pr, winrate, style
    [0,0,'bad'],
    [900,45, 'belowAverage'],
    [1200,48, 'average'],
    [1400,52, 'aboveAverage'],
    [1600,54, 'good'],
    [1800,56, 'veryGood'],
    [2000,60, 'unicum'],
    [2200,65, 'superUnicum']
];

export const getColorFromStat = (stat:number, isPr:boolean = true):string => {
    const i = isPr ? 0:1;
    let level:string =  String(prStyleTreshold[0][2]);
    prStyleTreshold.forEach(el => {
        if(el[i] <= stat) {
            level = String(el[2]);
        }
    });
    return level;
};

export const PrDisplay = (props:any) => <span className={getColorFromStat(props.pr)}>props.text</span>;
export const WrDisplay = (props:any) => <span className={getColorFromStat(props.wr, false)}>{props.wr.toFixed(2)}%</span>

export const PlayerDetailled = (props:any) => {

    return (
        <div className='player'>
            Games : {props.games.length}
            Pr : <PrDisplay pr={props.pr} text={props.pr} />
            Wr : <WrDisplay wr={(props.wins / props.games.length) * 100} />
        </div>
    )


}