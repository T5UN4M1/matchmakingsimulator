import {Rank} from "./rank/Rank";
import {Game, GameStatus} from "./Game";

class Player {
    pr : number

    rank : Rank;
    games : Game[] = [];
    wins : number = 0;


    constructor(pr:number, rank:Rank){
        this.pr = pr;
        this.rank = rank;
    }

    addGame(game:Game) {
        this.games.push(game);
        if((game.team1.includes(this) && game.result === GameStatus.TEAM_1) || (game.team2.includes(this) && game.result === GameStatus.TEAM_2)) {
            ++this.wins;
        }
    }

    getWr(formatted:boolean = false) : (number|string) {
        const wr = (this.wins / this.games.length) * 100;
        return formatted ? wr.toFixed(2) : wr;
    }
}

export {Player}