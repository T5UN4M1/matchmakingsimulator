import {Player} from "./Player";
import {Game} from "./Game";
import {generatePlayers, GenerationType} from "./PlayerGenerator";
import {WowsRankS72022, WowsRankS72022Leagues} from "./rank/WowsRankS72022";
import {ProcessResult} from "./rank/GameResultProcess";
import {shuffleArray} from "../service/util";

class MMParams {
    playersPerTeam: number = 0;
    playerAmount : number = 0;

    constructor(playersPerTeam: number, playerAmount : number) {
        if(playerAmount%(playersPerTeam*2)){
            // because otherwise we will have players without a game in each wave of simulation
            // actually we don't care, players will be divided in mm pools and we will have some players that don't play in each iteration
            //throw new Error("playerAmount is incorrect, must be a multiple of playersPerTeam*2");
        }

        this.playersPerTeam = playersPerTeam;
        this.playerAmount = playerAmount;
    }
}


class MatchMakerSystem {
    players : Player[] = [];
    games : Game[] = [];

    playersPerTeam: number;

    constructor(params: MMParams){
        this.players = generatePlayers(300,2200,params.playerAmount,GenerationType.LINEAR);

        this.playersPerTeam = params.playersPerTeam;
    }


    playIterationWowsS72022() {
        let bronzePool: Player[] = [],
            silverPool: Player[] = [],
            goldPool: Player[] = [],
            topPool: Player[] = [];

        shuffleArray(this.players);
        this.players.sort((a,b) => a.games.length - b.games.length);
        bronzePool = this.players.filter(p => (p.rank as WowsRankS72022).getMMLeague() === WowsRankS72022Leagues.BRONZE);
        silverPool = this.players.filter(p => (p.rank as WowsRankS72022).getMMLeague() === WowsRankS72022Leagues.SILVER);
        goldPool = this.players.filter(p => (p.rank as WowsRankS72022).getMMLeague() === WowsRankS72022Leagues.GOLD);
        topPool = this.players.filter(p => (p.rank as WowsRankS72022).getMMLeague() === WowsRankS72022Leagues.TOP);

        [bronzePool, silverPool, goldPool].forEach((pool: Player[]) => {
            const gamesAmount = ~~(pool.length / (this.playersPerTeam*2));
            [...Array(gamesAmount).keys()].forEach(i => {
                const game = new Game(
                    pool.slice(i*this.playersPerTeam*2, i*this.playersPerTeam*2 + this.playersPerTeam),
                    pool.slice(i*this.playersPerTeam*2 + this.playersPerTeam,(i+1)*this.playersPerTeam*2)
                );
                game.playGame();
                game.addGameToPlayersHistory();
                ProcessResult.processWowsRankS72022(game);
                this.games.push(game);
            })
        });
    }
}

export {MatchMakerSystem, MMParams}