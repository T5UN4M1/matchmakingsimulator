import {Player} from "./Player";
import {clamp, shuffleArray} from "../service/util";
import {WowsRankS72022} from "./rank/WowsRankS72022";

enum GenerationType {
    LINEAR// generate players with a linear skill level, so if we generate 150 players , we will have 50 bad players, 50 avg players and 50 good players
}

function generatePlayers(minPr: number, maxPr: number, amount: number, type : GenerationType): Player[] {
    switch(type) {
        case GenerationType.LINEAR:
            return generatePlayersLinear(minPr, maxPr, amount);
    }
}

function generatePlayersLinear(minPr: number, maxPr: number, amount: number): Player[] {
    const step = (maxPr - minPr) / amount;

    return shuffleArray([...Array(amount).keys()]
        .map(n => new Player(clamp(minPr + n * step, minPr,maxPr), new WowsRankS72022())));
}


export {GenerationType, generatePlayers}