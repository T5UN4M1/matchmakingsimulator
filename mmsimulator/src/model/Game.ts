import {Player} from "./Player";
import {getRng} from "../service/util";
enum GameStatus {
    TEAM_1,TEAM_2,NOT_PLAYED
}
class Game {
    team1 : Player[] = [];
    team2 : Player[] = [];
    result : GameStatus;

    // one entry for each player, organized like this :
    // [team1.player1,team1.player2,team1.player3, ... , team2.player1,team2. player2,...]
    playerPerformance : number[] = [];

    constructor(team1:Player[], team2: Player[]){
        if(team1.length !== team2.length) {
            throw new Error("Teams must be of equal size");
        }
        this.team1 = team1;
        this.team2 = team2;
        this.result = GameStatus.NOT_PLAYED;
        this.playerPerformance = Array(team1.length * 2).fill(0);
    }

    playGame() {
        const computeContribution = (pr:number): number => ((300 + pr) * (getRng(50,150) / 100));

        let score1 = 0;
        let score2 = 0;
        const goal = 100000;
        for(;(score1 < goal && score2 < goal) || score1 === score2;){
            this.team1.forEach((player:Player,i:number) => {
                const contribution = computeContribution(player.pr);
                score1 += contribution;
                this.playerPerformance[i] += contribution;
            });

            this.team2.forEach((player:Player,i:number) => {
                const contribution = computeContribution(player.pr);
                score2 += contribution;
                this.playerPerformance[this.team1.length + i] += contribution;
            });
        }
        this.result = (score1 > score2) ? GameStatus.TEAM_1 : GameStatus.TEAM_2;
    }
    addGameToPlayersHistory() {
        [...this.team1,...this.team2].forEach((p:Player) => p.addGame(this));
    }
}

export {Game, GameStatus}