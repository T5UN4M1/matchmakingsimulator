import {Game} from "../Game";


abstract class Rank {
    name : string = 'No rank';

    protected constructor(name: string = "unranked"){
        this.name = name;
    }

    abstract compare(rank : Rank): number;
    abstract canPlayWith(rank : Rank): boolean;

}

export {Rank}