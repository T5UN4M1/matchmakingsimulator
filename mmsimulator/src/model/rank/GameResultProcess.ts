import {Game, GameStatus} from "../Game";
import {WowsRankS72022, WowsRankS72022Leagues} from "./WowsRankS72022";
import {Player} from "../Player";


enum RankSystem {
    WowsRankS72022,
    EloSystem,
    LolSystem
}

const ProcessResult = {
    processWowsRankS72022 : (game: Game) => {
        if(game.result === GameStatus.NOT_PLAYED) {
            throw new Error("game is not played");
        }
        const league = (game.team1[0].rank as WowsRankS72022).getMMLeague();

        let topPerformersShield : number;
        switch(league){
            case WowsRankS72022Leagues.BRONZE:
                topPerformersShield = 2;
                break;
            case WowsRankS72022Leagues.SILVER:
                topPerformersShield = 1;
                break;
            default :
                topPerformersShield = 0;
                break;
        }
        const removeStarForLowPerformers = (team : Player[], performances : number[]) => team
            .map((player: Player,i) => ({player : player, performances : performances[i]}))
            .sort((a,b) => b.performances - a.performances) // sorting desc
            .filter((item, i) => i >= topPerformersShield) // removing good performers
            .forEach(item => (item.player.rank as WowsRankS72022).removeStar()); // removing stars for low performers

        const addStarForWinners = (team: Player[]) => team.forEach((player: Player) => (player.rank as WowsRankS72022).giveStar());

        if(game.result === GameStatus.TEAM_1) {
            const perfs = game.playerPerformance.slice(game.playerPerformance.length / 2);
            removeStarForLowPerformers(game.team2, perfs);
            addStarForWinners(game.team1);
        } else {
            removeStarForLowPerformers(game.team1, game.playerPerformance);
            addStarForWinners(game.team2);
        }
    }
}


export {RankSystem, ProcessResult}