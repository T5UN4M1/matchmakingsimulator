import {Rank} from "./Rank";

const internalData = {
    starsTresholds: { // how many stars to get to higher rank
        bronze: [1, 1, 1, 2, 2, 2, 2, 3, 3],
        silver: [1, 1, 2, 2, 2, 2, 3, 3,3],
        gold: [1, 1, 2, 2, 2, 2, 3, 3, 3]
    },
    qualification: { // how many stars to qualify to next league
        toSilver: 6,
        toGold: 5
    },
    cantLooseRank: { // can't lose rank at these ranks
        bronze: [10, 9, 8, 5],
        silver: [10, 6],
        gold: [10]
    },
    leagueNames: ["bronze", "silver", "gold", "top"],

}

enum League {
    BRONZE = 0, SILVER, GOLD, TOP
};

class WowsRankS72022 extends Rank {
    static leagues = League;

    league: League;
    division: number;
    starCount: number;

    constructor(league: League = League.BRONZE, division: number = 10, starCount: number = 0) {
        super();
        this.starCount = starCount
        this.division = division
        this.league = league;

        this.name = this.getName();
    }

    getName(): string {
        return `${internalData.leagueNames[this.league]} ${this.division} ${this.starCount} star(s)`
    }

    getMMLeague(): League {
        return (this.division === 1) ? WowsRankS72022.getUpperLeague(this.league) : this.league; // player in 1st div get matched with higher league for qual
    }
    promoteLeague() {
        this.starCount = 0;
        this.division = 10;
        this.league = WowsRankS72022.getUpperLeague(this.league);
        if(this.league === League.TOP) {
            this.division = 1;
        }
    }
    promoteDivision(){
        this.starCount = 0;
        --this.division;
    }
    giveStar() {
        ++this.starCount;

        if (this.division === 1) { // promotions of league
            if (
                (this.league === League.BRONZE && this.starCount === internalData.qualification.toSilver) ||
                (this.league === League.SILVER && this.starCount === internalData.qualification.toGold)
            ) {
                this.promoteLeague();
            }
        } else {
            if(
                (this.league === League.BRONZE && this.starCount === internalData.starsTresholds.bronze[10-this.division]) ||
                (this.league === League.SILVER && this.starCount === internalData.starsTresholds.silver[10-this.division]) ||
                (this.league === League.GOLD && this.starCount === internalData.starsTresholds.gold[10-this.division])
            ){
                this.promoteDivision();
            }
        }
    }

    removeStar() {
        if(this.starCount === 0){
            if(this.division === 1 || this.league === League.TOP) { // first div is qualification and is shielded
                return;
            }
            if(
                (this.league === League.BRONZE && internalData.cantLooseRank.bronze.includes(this.division)) ||
                (this.league === League.SILVER && internalData.cantLooseRank.silver.includes(this.division)) ||
                (this.league === League.GOLD && internalData.cantLooseRank.gold.includes(this.division))
            ){ // division is shielded
                return;
            }
            // at this point we need to lose division and regain stars (treshold - 1)
            ++this.division;
            switch(this.league) {
                case League.BRONZE:
                    this.starCount = internalData.starsTresholds.bronze[10 - this.division] - 1;
                    break;
                case League.SILVER:
                    this.starCount = internalData.starsTresholds.silver[10 - this.division] - 1;
                    break;
                case League.GOLD:
                    this.starCount = internalData.starsTresholds.gold[10 - this.division] - 1;
                    break;
            }

        } else {
            --this.starCount;
        }
    }

    static getUpperLeague(league: League) {
        switch (league) {
            case League.BRONZE:
                return League.SILVER;
            case League.SILVER:
                return League.GOLD;
            case League.GOLD:
                return League.TOP;
            case League.TOP:
                return League.TOP;
        }
    }

    static convertRank(rank: Rank): WowsRankS72022 {
        if (!(rank instanceof WowsRankS72022)) {
            throw new Error("Different types of ranks");
        }
        return rank as WowsRankS72022;
    }

    canPlayWith(rank: Rank): boolean {

        const otherRank: WowsRankS72022 = WowsRankS72022.convertRank(rank);

        return this.getMMLeague() === otherRank.getMMLeague();
    }

    compare(rank: Rank): number {
        const otherRank: WowsRankS72022 = WowsRankS72022.convertRank(rank);

        return this.toNumber() - otherRank.toNumber();
    }

    toNumber(): number {
        return this.league * 1000000 + (10 - this.division) * 1000 + this.starCount;
    }

}

export {WowsRankS72022, League as WowsRankS72022Leagues}