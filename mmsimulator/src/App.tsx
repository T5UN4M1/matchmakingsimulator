import React from 'react';
import './App.css';
import {Main} from "./components/Main";
import createTheme from "@mui/material/styles/createTheme";
import ThemeProvider from "@mui/material/styles/ThemeProvider";

const darkTheme = createTheme({
    palette: {
        mode: 'dark',
    },
});

function App() {
    return (
        <ThemeProvider theme={darkTheme}>
            <div className="App">
                <Main></Main>
            </div>
        </ThemeProvider>
    );
}

export default App;
